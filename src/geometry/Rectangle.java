package geometry;

public class Rectangle implements Shape {
	 private double width;
	 private double length;
	 
	 public Rectangle(double width, double length) {
		 
		 if ( width <=0 || length <= 0) {
			 
			 throw new IllegalArgumentException("Length and width must be greater than zero");
 
		 }
		 this.width = width;
		 this.length = length;
	 }
	 
	 public double getWidth() {
		 return width;
	 }
	 
	 public double getLength() {
		 return length;
	 }
	 
	 public double getArea() {
		 return width*length;
	 }
	 
	 public double getPerimeter() {
		 return 2*(width+length);
	 }

}
