package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		
		Shape [] shapes = new Shape[5];
		
	
		
		try {
		shapes[0] = new Circle(2);
		shapes[1] = new Circle(2);
		shapes[2] = new Rectangle(3,7);
		shapes[3] = new Rectangle(5,5);
		shapes[4] = new Square(6);
		}
		
	
		
		catch(IllegalArgumentException e) {
			System.out.println("inputs must be greater than zero");
		}
		
		try {
			
		for(Shape s : shapes) {
			System.out.println(
			" Area="+s.getArea() +
			" , Perimeter="+s.getPerimeter());
		}
		
		}
			catch(NullPointerException e) {
			System.out.println(" Not all object are created: START AGAIN!!");
			
		}

	}

}
