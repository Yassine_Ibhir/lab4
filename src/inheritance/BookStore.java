package inheritance;

public class BookStore {

	public static void main(String[] args) {
		
		Book [] books = new Book[5];

		
		try {
		
		books[0] = new Book ("A long long way","Barry");
		books[2] = new Book ("Venus and Adonis", "Shakespear");
		books[1] = new ElectronicBook("DFG","Bob",(byte) 4);
		books[3] = new ElectronicBook("ZYX","Alice",(byte) 4);
		books[4] = new ElectronicBook("vdfgdf","Who?",(byte) 4);
		
		}
		
		catch(IllegalArgumentException e) {
			System.out.println("One or more of the arrays "
					+ "book object has not been created. "
					+ "Author and title length must be greater than 0.");
		}
		
		
			for(Book b : books) {
				
				System.out.println(b);
			
			}

		
	}

}
