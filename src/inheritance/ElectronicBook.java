package inheritance;

public class ElectronicBook extends Book {
	private byte numberBytes;
	
	public ElectronicBook(String title,String author, byte numberBytes){
		
		super(title,author);
		
		this.numberBytes = numberBytes;
	
	}
	
	//override
	public String toString() {
		return super.toString()+ ", NumberBytes: " + numberBytes;
	}
	
		
}
